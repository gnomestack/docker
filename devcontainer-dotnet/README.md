# devcontainer-dotnet

This container is for building dotnet projects that are multi-targeted for
the dotnet core sdk and mono. It includes deno, pwsh, qtr to run tasks
and nvs for the case where node or multiple versions of node is needed.

Telemetry is disabled for pwsh and dotnet.
